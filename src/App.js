import './App.css';
import PunchLocation from './PunchLocation';
import { useState } from 'react';

function App() {
  const [place,setPlace]= useState(undefined)
  return (
    <div className="App">
      <header className="App-header">
      <PunchLocation geocode={true} selected={place} onSelect={(place)=>{setPlace(place)}} label="Start Location" placeholder="Start Location"/>
      </header>
    </div>
  );
}

export default App;
