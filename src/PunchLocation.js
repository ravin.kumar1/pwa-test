import React, { useState } from 'react'
export default function PunchLocation({ selected, onSelect, label }) {
    const [loading, setLoading] = useState(false)
    const [data, setData] = useState(undefined)


    const position = async () => {
        setLoading(true)
         navigator.geolocation.getCurrentPosition(
            position => {
                console.log("position",position);
                alert("Coordinates get successfully")
                setLoading(false)
                setData(position.coords.latitude)
            },
            err => {
                alert("Unable to fetch your current position")
                setLoading(false)
            }
        );
    }
    return (
        <>
            <p className="text-tiny text-small">
                {"Location"}
            </p>
                {data && <div>Lat:{data}</div>}
            {
                loading ? <div>Loading...</div> :  <a
                className="App-link"
                onClick={position}
                target="_blank"
                rel="noopener noreferrer"
              >
                Open 
              </a>
            }
        </>
    )
}
